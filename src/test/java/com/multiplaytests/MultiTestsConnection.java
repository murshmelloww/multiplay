package com.multiplaytests;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class MultiTestsConnection {

    @LocalServerPort
    public Integer port;

    public RequestSpecification request;

    public static JSONObject data;

    protected String testPath = "/Users/apple/IdeaProjects/multiplay/src/test/java/com/multiplaytests/";

    @BeforeEach
    public void createRestRequest() {
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost:" + port;
    }



}
