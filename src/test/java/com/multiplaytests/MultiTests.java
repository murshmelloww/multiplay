package com.multiplaytests;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import static com.multyplay.logic.Calculation.calculate;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiTests extends MultiTestsConnection {

    MultiplayCreateRequests multiplayRequests;

    @BeforeEach
    public void makeFast() {
        multiplayRequests = new MultiplayCreateRequests(request);
    }

    @Test
    public void calc () throws JSONException {
//        JSONObject calcResult = new JSONObject(calculate(7));
//        JSONObject calcExp = new JSONObject("{ \"1\": 7,  \"2\": 49,  \"3\": \"343\", \"4\": \"2401\"}");
//        assertEquals(calcResult, calcExp);
        multiplayRequests.calculate(3).assertThat().statusCode(200);
    }

}
