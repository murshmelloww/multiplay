package com.multiplaytests;

import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class MultiplayCreateRequests {

    private RequestSpecification request;

    public MultiplayCreateRequests(RequestSpecification request){
        request.contentType("application/json");
        this.request = request;
    }

    public ValidatableResponse calculate(Integer testData) {
        return request
                .get("/calculate/value="+ testData)
                .then();
    }
}
