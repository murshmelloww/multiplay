package com.multyplay.endpoints;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.multyplay.logic.Calculation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.awt.*;

@RestController
public class Multy {

    @GetMapping(path="/calculate")
    @ResponseBody
    public String calculate(@RequestParam(name= "value") int number) {
        return Calculation.calculate(number);
    }
}
